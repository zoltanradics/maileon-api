MaileonApi
=====================

MaileonApi is a class built for easily communicate with Maileon CRM. It provides the two basic actions (subscribe, unsubscribe) for manage subscribers.

### Subscribe

##### The form

	<form action="{{ app.url_generator.generate('subscribeAction') }}" method="post" role="form">

	  <div class="form-group">
	    <label for="email">E-mail</label>
	    <input id="email" class="form-control" type="email" name="email" placeholder="E-mail" value="">
	  </div>

	  <!-- Maileon works with standard and custom fields. Please check documentation on this -->

	  <div class="form-group">
	    <label for="last_name">First Name</label>
	    <input id="last_name" class="form-control" type="text" name="standard_fields[FIRSTNAME]" placeholder="First Name" value="">
	  </div>

	  <div class="form-group">
	    <label for="last_name">Last Name</label>
	    <input id="last_name" class="form-control" type="text" name="standard_fields[LASTNAME]" placeholder="Last Name" value="">
	  </div>

	  <input type="hidden" name="redirect" value="{{ app.url_generator.generate('contact_subscribe') }}">
	  <button type="submit" class="btn btn-default">Submit</button>
	</form>


##### The api call

    $maileonApiCall = new MaileonApi($app);
    $maileonApiCallResult = $maileonApiCall->callApi(array(
      'action' => 'subscribe',
      'postData' => $postData, // an assoc. array
      'attributes' => array(
        'permission' => 1,
        'sync_mode' => 1,
        'src' => false,
        'subscription_page' => false,
        'doi' => false,
        'doiplus' => false,
        'doimailing' => false
      )
    ));

### Unsubscribe

##### The form

	<form action="{{ app.url_generator.generate('unsubscribeAction') }}" method="post" role="form">

	  <div class="form-group">
	    <label for="email">E-mail</label>
	    <input id="email" class="form-control" type="email" name="email" placeholder="E-mail" value="">
	  </div>

	  <input type="hidden" name="redirect" value="{{ app.url_generator.generate('unsubscribe_route') }}">
	  <button type="submit" class="btn btn-default">Submit</button>

	</form>

##### The api call

	$maileonApiCall = new MaileonApi($app);
    $maileonApiCallResult = $maileonApiCall->callApi(array(
      'action' => 'unsubscribe',
      'postData' => $postData // an assoc. array
    ));

### Appears

- Bitbucket https://bitbucket.org/zoltanradics/maileon-api
- Packagist https://packagist.org/packages/geometry/maileon-api

### Lear more about Maileon API 1.0

Checkout detailed docs for Maileon API 1.0 to fine tune your API calls:
[http://dev.maileon.com/api/rest-api-1-0/?lang=en](http://dev.maileon.com/api/rest-api-1-0/?lang=en)
