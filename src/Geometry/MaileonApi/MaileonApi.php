<?php 

namespace Geometry\MaileonApi;

/**
  MaileonApi 
**/

class MaileonApi
{

  private $app;
  
  function __construct($app)
  {
    $this->app = $app;
  }

  public function callApi($arguments) 
  {
    # Set variables
    $action = $arguments['action'];

    # Change twig path
    $this->app['twig.loader.filesystem']->addPath(
      '../vendor/geometry/maileon-api/src/Geometry/MaileonApi/views/'
    );

    $xml = false;
    if ( isset($arguments['postData']['standard_fields']) && isset($arguments['postData']['custom_fields']) )
      $xml = $this->app['twig']->render('data.twig.xml', $arguments['postData']);

    # Set action config
    $actionConfig = array(
      'action' => $arguments['action'],
      'apiKey' => $this->app['apiKey'],
      'apiUrl' => $this->app['apiUrl'],
      'postData' => $arguments['postData'],
      'attributes' => $arguments['attributes'],
      'xml' => $xml
    );

    # Handle call
    return $this->$action($actionConfig);
  }

  private function subscribe($arguments) 
  {
    # Setup the API URL
    $url = $this->app['apiUrl'] . 'contacts' . '/' . $arguments['postData']['email'];

    # Check if attributes are exists and complete the URL with parameters
    if ( isset($arguments['attributes']) && count($arguments['attributes']) > 0 )
      $url .= '?' . http_build_query($arguments['attributes']);

    # Add URL completed URL to $arguments
    $arguments['url'] = $url;
    $arguments['method'] = 'POST';
    $arguments['expectedStatusCode'] = 201;

    # Let's do the curl call
    return $this->curl($arguments);
  }

  private function unsubscribe($arguments) 
  { 
    # Setup the API URL
    $url = $this->app['apiUrl'] . 'contacts' . '/' . $arguments['postData']['email'] . '/' . 'unsubscribe';

    # Add URL completed URL to $arguments
    $arguments['url'] = $url;
    $arguments['method'] = 'DELETE';
    $arguments['expectedStatusCode'] = 204;

    # Let's do the curl call
    return $this->curl($arguments);
  }

  private function getcontact($arguments) 
  {
    # Setup the API URL
    $url = $this->app['apiUrl'] . 'contacts' . '/' . 'contact';

    # Check if attributes are exists and complete the URL with parameters
    if (isset($arguments['attributes']) && count($arguments['attributes'])>0 ):
      $c = 0;
      foreach ($arguments['attributes'] as $key => $value):
        $c++;

        if (!$value)
          break;

        # Before the first parameter put '?'
        if ($c==1):
          $url .= '?';
        else:
          $url .= '&';
        endif;

        # Add the parameters to the URL
        if (is_array($value)):
          $d = 0;
          foreach ($value as $subvalue):
            $d++;

            if (!$subvalue)
              break;

            # Before the first parameter put '?'
            if ($d>1)
              $url .= '&';

            $url .= $key . '=' . $subvalue;
          endforeach;
        else:
          $url .= $key . '=' . $value;
        endif;

      endforeach;
    endif;

    # Add URL completed URL to $arguments
    $arguments['url'] = $url;
    $arguments['method'] = 'GET';
    $arguments['expectedStatusCode'] = 200;

    # Let's do the curl call
    return $this->curl($arguments);
  }


  private function contactexists($arguments)
  {
    # Setup the API URL
    $url = $this->app['apiUrl'] . 'contacts/email/' . urlencode($arguments['attributes']['email']);

    # Add URL completed URL to $arguments
    $arguments['url'] = $url;
    $arguments['method'] = 'GET';
    $arguments['expectedStatusCode'] = 200;

    # Let's do the curl call
    return $this->curl($arguments);
  }

  private function curl($arguments)
  {
    # Setup header for the request
    $headers = array (
      "Content-Type: application/vnd.maileon.api+xml; charset=utf-8",
      'Expect:',
      "Authorization: Basic " . base64_encode($arguments['apiKey'])
    );

    # Config cURL
    $handle = curl_init();
    curl_setopt ( $handle, CURLOPT_URL, $arguments['url']);
    curl_setopt ( $handle, CURLOPT_CUSTOMREQUEST, $arguments['method']);
    curl_setopt ( $handle, CURLOPT_HTTPHEADER, $headers);
    curl_setopt ( $handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt ( $handle, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt ( $handle, CURLOPT_SSL_VERIFYPEER, false);

    # Add curl_setopts depend on method
    switch ($arguments['method']):
      case 'POST':
        curl_setopt ( $handle, CURLOPT_POST, true);
        curl_setopt ( $handle, CURLOPT_POSTFIELDS, $arguments['xml']);
      break;
    endswitch;
    
    $state = curl_exec($handle);

    # Get status code
    $statusCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

    # Set return depends on status code
    if ($statusCode == $arguments['expectedStatusCode']):
      $return = array('success' => true);
      # If state is exist, push data to return array
      if ($state):
        $return['data'] = $state;
      endif;
    else:
      $return = array(
        'success' => false,
        'code' => $statusCode
      );
    endif;

    # Return the result
    return $return;
  }

}
